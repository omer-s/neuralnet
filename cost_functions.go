package neuralnet

type MeanSquaredErrorCostFunction struct {
}

func (f MeanSquaredErrorCostFunction) CalculateTotal(a []float64, y []float64) float64 {
	totalCost := 0.0
	for i := range a {
		delta := a[i] - y[i]
		totalCost += (delta * delta)
	}
	return 0.5 * (totalCost / float64(len(a)))
}

func (f MeanSquaredErrorCostFunction) OutputError(z []float64, a []float64, y []float64, activator Activator) (outputErrors []float64) {
	outputErrors = make([]float64, len(a))
	for i := range a {
		outputErrors[i] = (a[i] - y[i]) * activator.Derivative(z[i])
	}
	return
}

type CrossEntropyCostFunction struct {
}

// func (f CrossEntropyCostFunction) CalculateTotal(a []float64, y []float64) float64 {
// 	totalCost := 0.0
// 	for i := range a {
// 		delta := a[i] - y[i]
// 		totalCost += (delta * delta)
// 	}
// 	return 0.5 * (totalCost / float64(len(a)))
// }

// func (f CrossEntropyCostFunction) Derivative(a []float64, y []float64) (result []float64) {
// 	result = make([]float64, len(a))
// 	for i := range a {
// 		result[i] = a[i] - y[i]
// 	}
// 	return
// }

// C=−1n∑x[ylna+(1−y)ln(1−a)]

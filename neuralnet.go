package neuralnet

import (
	"bufio"
	"bytes"
	"encoding/binary"
	"encoding/gob"
	"fmt"
	"log"
	"math/rand"
	"os"
)

// use exported fields for serialization
type NeuralNet struct {
	LayerSizes []int
	//layer X neurons in layer X w for each neuron layer+1:
	Weights [][][]float64
	// bias for each neuron
	Biases [][]float64

	Activator    Activator
	CostFunction CostFunction

	LearningRate float64
}

type Activator interface {
	Activate(float64) float64
	Derivative(float64) float64
}

type CostFunction interface {
	CalculateTotal([]float64, []float64) float64
	OutputError(z []float64, a []float64, y []float64, activator Activator) []float64
}

type LabeledData struct {
	X [][]float64
	Y [][]float64
}

func (neuralNet *NeuralNet) LayerCount() int {
	return len(neuralNet.LayerSizes)
}

func (neuralNet *NeuralNet) OutputLayer() int {
	return len(neuralNet.LayerSizes) - 1
}

func (neuralNet *NeuralNet) OutputCount() int {
	return neuralNet.LayerSizes[neuralNet.OutputLayer()]
}

func NewNeuralNet(layerSizes []int, activator Activator, costFunction CostFunction, learningRate float64) (neuralNet NeuralNet) {

	neuralNet = NeuralNet{}
	neuralNet.Activator = activator
	neuralNet.CostFunction = costFunction
	neuralNet.LearningRate = learningRate
	neuralNet.LayerSizes = layerSizes
	neuralNet.Weights, neuralNet.Biases = neuralNet.allocateWeights()
	return
}

func (neuralNet *NeuralNet) Serialize(path string) {
	var b bytes.Buffer

	gob.Register(neuralNet)
	gob.Register(neuralNet.Activator)
	gob.Register(neuralNet.CostFunction)

	// Create a new gob encoder and use it to encode the person struct
	enc := gob.NewEncoder(&b)
	if err := enc.Encode(neuralNet); err != nil {
		fmt.Println("Error encoding struct:", err)
		return
	}

	serializedData := b.Bytes()

	f, err := os.Create(path)
	if err != nil {
		fmt.Println("Couldn't open file")
	}
	defer f.Close()

	err = binary.Write(f, binary.LittleEndian, serializedData)
	if err != nil {
		log.Fatal("Write failed")
	}
}

func (neuralNet *NeuralNet) Deserialize(path string) {

	gob.Register(neuralNet)
	gob.Register(neuralNet.Activator)
	gob.Register(neuralNet.CostFunction)

	file, err := os.Open(path)
	if err != nil {
		fmt.Println("Couldn't open file")
	}
	defer file.Close()

	bufr := bufio.NewReader(file)
	enc := gob.NewDecoder(bufr)
	if err := enc.Decode(neuralNet); err != nil {
		fmt.Println("Error decoding struct:", err)
		return
	}
}

func (neuralNet *NeuralNet) allocateWeights() (weights [][][]float64, biases [][]float64) {

	weights = make([][][]float64, len(neuralNet.LayerSizes)-1)
	biases = make([][]float64, len(neuralNet.LayerSizes))

	for layer := 0; layer < len(neuralNet.LayerSizes)-1; layer++ { //-1: no weights needed for output layer
		neuronsInLayer := neuralNet.LayerSizes[layer]
		neuronsInNextLayer := neuralNet.LayerSizes[layer+1]
		weights[layer] = make([][]float64, neuronsInLayer)
		for n := 0; n < neuronsInLayer; n++ {
			weights[layer][n] = make([]float64, neuronsInNextLayer)
		}
	}

	//allocate biases; values in input layer not used but still allocating to keep operations simpler
	for layer := 0; layer < len(neuralNet.LayerSizes); layer++ {
		neuronsInLayer := neuralNet.LayerSizes[layer]
		biases[layer] = make([]float64, neuronsInLayer)
	}

	return
}

// Train the nueral network given training instances (x) and output(y)
func (neuralNet *NeuralNet) Train(trainingData *LabeledData, testData *LabeledData, batchSize int, iterations int) {

	//shuffle training data
	for i := range trainingData.X {
		j := rand.Intn(i + 1)
		trainingData.X[i], trainingData.X[j] = trainingData.X[j], trainingData.X[i]
		trainingData.Y[i], trainingData.Y[j] = trainingData.Y[j], trainingData.Y[i]
	}

	//randomly initialize weights with normal distribution
	for i := range neuralNet.Weights {
		for j := range neuralNet.Weights[i] {
			for k := range neuralNet.Weights[i][j] {
				neuralNet.Weights[i][j][k] = rand.NormFloat64()
			}
		}
	}

	for i := range neuralNet.Biases {
		for j := range neuralNet.Biases[i] {
			neuralNet.Biases[i][j] = rand.NormFloat64()
		}
	}

	for i := 0; i < iterations; i++ {
		fmt.Printf("Iteration:%d ", i)
		neuralNet.evaluate(testData)
		batchStart := 0
		for batchStart < len(trainingData.X) {
			batch := trainingData.Slice(batchStart, batchStart+batchSize)
			neuralNet.learnFromBatch(batch)
			batchStart += batchSize
		}
	}

	fmt.Print("Training finished. ")
	neuralNet.evaluate(testData)
}

func (neuralNet *NeuralNet) evaluate(testData *LabeledData) {

	predictions := neuralNet.Predict(testData)

	totalCost := 0.0
	matchCount := 0
	for i := range testData.Y {

		totalCost += neuralNet.CostFunction.CalculateTotal(testData.Y[i], predictions[i])

		outputMatches := true
		for j := 0; j < neuralNet.OutputCount(); j++ {
			prediction := predictions[i][j]
			predictionBinary := int(prediction / 0.5)
			if predictionBinary != int(testData.Y[i][j]) {
				outputMatches = false
				break
			}
		}
		if outputMatches {
			matchCount++
		}
	}

	cost := totalCost / float64(len(testData.Y))
	accuracy := float64(matchCount) / float64(len(testData.Y))
	fmt.Printf("Cost: %f Accuracy: %v\n", cost, accuracy)
}

func (neuralNet *NeuralNet) learnFromBatch(batch LabeledData) {

	weightedSums := make([][]float64, neuralNet.LayerCount())
	activations := make([][]float64, neuralNet.LayerCount())
	errorRates := make([][]float64, neuralNet.LayerCount())
	weightDeltas, biasDeltas := neuralNet.allocateWeights()

	for layer := range neuralNet.LayerSizes {
		layerSize := neuralNet.LayerSizes[layer]
		weightedSums[layer] = make([]float64, layerSize)
		activations[layer] = make([]float64, layerSize)
		errorRates[layer] = make([]float64, layerSize)
	}

	outputLayer := neuralNet.OutputLayer()
	for i := range batch.X {
		neuralNet.forwardPropagate(batch.X[i], weightedSums, activations)
		neuralNet.calculateOutputError(activations[outputLayer], weightedSums[outputLayer], batch.Y[i], &errorRates[outputLayer])
		neuralNet.backPropagate(weightedSums, activations, errorRates)
		neuralNet.updateWeightDeltas(activations, errorRates, weightDeltas, biasDeltas, i)
	}
	neuralNet.updateWeights(weightDeltas, biasDeltas)
}

func (neuralNet *NeuralNet) Predict(testData *LabeledData) [][]float64 {

	results := make([][]float64, len(testData.X))

	weightedSums := make([][]float64, neuralNet.LayerCount())
	activations := make([][]float64, neuralNet.LayerCount())
	for layer := range neuralNet.LayerSizes {
		layerSize := neuralNet.LayerSizes[layer]
		weightedSums[layer] = make([]float64, layerSize)
		activations[layer] = make([]float64, layerSize)
	}

	for i, testInstance := range testData.X {
		neuralNet.forwardPropagate(testInstance, weightedSums, activations)
		results[i] = make([]float64, neuralNet.OutputCount())
		copy(results[i], activations[neuralNet.OutputLayer()])
	}

	return results
}

func (neuralNet *NeuralNet) forwardPropagate(x []float64, weightedSums [][]float64, activations [][]float64) {

	for layer := range activations { // iterate layers except input layer

		if layer == 0 { //input layer
			activations[0] = x
			continue
		}

		for neuron := 0; neuron < neuralNet.LayerSizes[layer]; neuron++ { // calculate activations for neurons in layer
			var weightedSum float64
			weightedSum += neuralNet.Biases[layer][neuron] * 1.0 // add bias weight
			for n := 0; n < neuralNet.LayerSizes[layer-1]; n++ { // add weighted sum from previous layer to current neuron
				weightedSum += neuralNet.Weights[layer-1][n][neuron] * activations[layer-1][n]
			}
			weightedSums[layer][neuron] = weightedSum
			activations[layer][neuron] = neuralNet.Activator.Activate(weightedSum)
		}
	}
}

func (neuralNet *NeuralNet) calculateOutputError(outputActivations []float64, outputWeightedSums []float64, labels []float64, outputErrors *[]float64) {

	*outputErrors = neuralNet.CostFunction.OutputError(outputWeightedSums, outputActivations, labels, neuralNet.Activator)
}

func (neuralNet *NeuralNet) backPropagate(weightedSums [][]float64, activations [][]float64, errorRates [][]float64) {

	for layer := neuralNet.OutputLayer() - 1; layer >= 0; layer-- {
		for nL1 := 0; nL1 < neuralNet.LayerSizes[layer]; nL1++ {
			var error float64
			for nL2 := 0; nL2 < neuralNet.LayerSizes[layer+1]; nL2++ {
				error += neuralNet.Weights[layer][nL1][nL2] * errorRates[layer+1][nL2] * neuralNet.Activator.Derivative(weightedSums[layer][nL1])
			}
			errorRates[layer][nL1] = error
		}
	}
}

func (neuralNet *NeuralNet) updateWeightDeltas(activations [][]float64, errorRates [][]float64, weightDeltas [][][]float64, biasDeltas [][]float64, instanceCount int) {

	for layer := 0; layer < neuralNet.LayerCount()-1; layer++ {
		// add biases
		for nL2 := 0; nL2 < neuralNet.LayerSizes[layer+1]; nL2++ {
			addToAverage(&biasDeltas[layer+1][nL2], errorRates[layer+1][nL2], instanceCount)
		}
		// add weights
		for nL1 := 0; nL1 < neuralNet.LayerSizes[layer]; nL1++ { // source neurons in l
			for nL2 := 0; nL2 < neuralNet.LayerSizes[layer+1]; nL2++ { // weights for target neurons in l2
				addToAverage(&weightDeltas[layer][nL1][nL2], errorRates[layer+1][nL2]*activations[layer][nL1], instanceCount)
			}
		}
	}
}

func (neuralNet *NeuralNet) updateWeights(weightDeltas [][][]float64, biasDeltas [][]float64) {

	for i := range neuralNet.Biases {
		for j := range neuralNet.Biases[i] {
			neuralNet.Biases[i][j] -= neuralNet.LearningRate * biasDeltas[i][j]
		}
	}

	for i := range neuralNet.Weights {
		for j := range neuralNet.Weights[i] {
			for k := range neuralNet.Weights[i][j] {
				neuralNet.Weights[i][j][k] -= neuralNet.LearningRate * weightDeltas[i][j][k]
			}
		}
	}
}

func addToAverage(avg *float64, newValue float64, count int) {
	*avg = ((*avg * float64(count)) + newValue) / (float64(count) + 1)
}

func (labeledData LabeledData) Slice(start int, end int) LabeledData {

	if end > len(labeledData.X) {
		end = len(labeledData.X)
	}

	return LabeledData{labeledData.X[start:end], labeledData.Y[start:end]}
}

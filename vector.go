package neuralnet

import "math/rand"

// Apply a function to all elements of the input vector
func Apply(v []float64, function func(value float64) float64) []float64 {
	result := make([]float64, len(v))
	for i := range v {
		result[i] = function(v[i])
	}
	return result
}

// Shuffle shuffles the order of elements in the vector
func Shuffle(v [][]float64) {
	for i := range v {
		j := rand.Intn(i + 1)
		v[i], v[j] = v[j], v[i]
	}
}

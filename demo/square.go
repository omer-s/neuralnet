package demo

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/omer-s/neuralnet"
)

// precision very low, not working yet

func Square() {

	rand.Seed(time.Now().Unix())

	//training input
	x := make([][]float64, 10000)
	//training output
	y := make([][]float64, 10000)

	train := make([]int, 40)
	for i := 0; i < 40; i++ {
		train[i] = rand.Int() % 50
		fmt.Print(train[i], " ")
	}

	// var val int
	// fmt.Scanf("%d\n", &val)

	for i := 0; i < 10000; i++ {
		index := rand.Int() % 40
		val := train[index]

		// fmt.Print(val, "\n")

		x[i] = make([]float64, 16)
		setBits(x[i], val)
		y[i] = make([]float64, 32)
		setBits(y[i], val*val)
	}

	// fmt.Scanf("%d\n", &val)

	trainingData := neuralnet.LabeledData{X: x, Y: y}
	neuralNet := neuralnet.NewNeuralNet(
		[]int{16, 32, 32},
		neuralnet.SigmoidActivator{},
		neuralnet.MeanSquaredErrorCostFunction{},
		3.0)

	neuralNet.Train(&trainingData, &trainingData, 500, 50)

	for {
		fmt.Print("#1: ")
		var val int
		fmt.Scanf("%d\n", &val)

		x := make([]float64, 16)
		setBits(x, int(val))
		y := make([]float64, 32)

		testData := neuralnet.LabeledData{X: [][]float64{x}, Y: [][]float64{y}}

		predictions := neuralNet.Predict(&testData)

		fmt.Println(val, " ^ 2 =", readBits(predictions[0]))
	}
}

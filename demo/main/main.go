package main

import (
	"math/rand"
	"time"

	"fmt"
	"os"

	"github.com/omer-s/neuralnet/demo"
)

func main() {

	rand.Seed(time.Now().Unix())

	if len(os.Args) != 2 {
		panic(fmt.Sprint("Invalid options, expected: [sum,xor]", os.Args))
	}

	if os.Args[1] == "sum" {
		demo.Sum()
	} else if os.Args[1] == "xor" {
		demo.Xor()
	} else if os.Args[1] == "sqr" {
		demo.Square()
	} else {
		panic("Invalid command, options: [sum,xor,sqr]")
	}
}

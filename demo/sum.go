package demo

import (
	"fmt"
	"math/rand"
	"time"

	"math"

	"github.com/omer-s/neuralnet"
)

func setBits(dest []float64, value int) {
	currentBit := 0
	for value > 0 {
		dest[currentBit] = float64(value % 2)
		value = value >> 1
		currentBit++
	}
}

func readBits(src []float64) int {
	value := 0.0
	for i, bit := range src {
		if bit >= 0.5 {
			value += math.Pow(2, float64(i))
		}
	}
	return int(value)
}

func Sum() {

	rand.Seed(time.Now().Unix())

	//training input
	x := make([][]float64, 256)
	//training output
	y := make([][]float64, 256)

	for i := 0; i < 16; i++ {
		for j := 0; j < 16; j++ {
			row := (i * 16) + j
			x[row] = make([]float64, 8)
			setBits(x[row][0:4], i)
			setBits(x[row][4:8], j)
			y[row] = make([]float64, 5)
			setBits(y[row][0:5], i+j)
		}
	}

	trainingData := neuralnet.LabeledData{X: x, Y: y}
	neuralNet := neuralnet.NewNeuralNet(
		[]int{8, 16, 5},
		neuralnet.SigmoidActivator{},
		neuralnet.MeanSquaredErrorCostFunction{},
		3.0)

	neuralNet.Train(&trainingData, &trainingData, 8, 300)

	for {
		fmt.Print("#1: ")
		var num1 int
		fmt.Scanf("%d\n", &num1)

		fmt.Print("#2: ")
		var num2 int
		fmt.Scanf("%d\n", &num2)

		x := make([]float64, 8)
		setBits(x[0:4], int(num1))
		setBits(x[4:8], int(num2))
		y := make([]float64, 5)

		testData := neuralnet.LabeledData{X: [][]float64{x}, Y: [][]float64{y}}

		predictions := neuralNet.Predict(&testData)

		fmt.Println(num1, "+", num2, "=", readBits(predictions[0]))
	}
}

package demo

import (
	"fmt"
	"math/rand"
	"time"

	"github.com/omer-s/neuralnet"
)

func Xor() {

	rand.Seed(time.Now().Unix())

	x := [][]float64{
		{0, 0},
		{0, 1},
		{1, 0},
		{1, 1},
	}
	//training output
	y := [][]float64{
		{0},
		{1},
		{1},
		{0},
	}

	trainingData := neuralnet.LabeledData{X: x, Y: y}
	neuralNet := neuralnet.NewNeuralNet(
		[]int{2, 4, 1},
		neuralnet.SigmoidActivator{},
		neuralnet.MeanSquaredErrorCostFunction{},
		2)

	neuralNet.Train(&trainingData, &trainingData, 1, 500)

	predictions := neuralNet.Predict(&trainingData)
	for i := range trainingData.Y {
		fmt.Println(trainingData.X[i], trainingData.Y[i], predictions[i])
	}
}

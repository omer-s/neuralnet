package neuralnet

import "math"

type SigmoidActivator struct {
}

// Activate computes sigmoid on given weight sum (z)
func (sigmoid SigmoidActivator) Activate(z float64) float64 {
	return 1 / (1 + math.Exp(-z))
}

// Derivative computes derivative of sigmoid
func (sigmoid SigmoidActivator) Derivative(z float64) float64 {
	a := sigmoid.Activate(z)
	return a * (1 - a)
}
